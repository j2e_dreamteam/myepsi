import fr.epsi.myepsi.blog.dal.ArticleDAO;
import fr.epsi.myepsi.blog.models.Article;
import fr.epsi.myepsi.blog.models.Statut;
import fr.epsi.myepsi.blog.models.Utilisateur;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

public class TestArticle {
    Article article;
    ArticleDAO articleDAO;
    Utilisateur utilisateur;
    @Test
    public void testGetArticleByStatut(){

    }

    @Test
    public void testDeleteArticle(){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(2019, 03, 21);
            String id="33898e23-90ce-481d-b44b-b4cbcd42fda7";
            String titre = "Je ne pense pas";
            String description = "Que j'y arriverai";
            String email = "simon.perols@epsi.fr";
            Date dateCreation = new java.sql.Date(calendar.getTimeInMillis());
            Date dateModification = new java.sql.Date(calendar.getTimeInMillis());
            Statut statut = new Statut(2, "Publi&eacute;");
            article = new Article(titre, description, email, dateCreation, dateModification, statut);
            article.setId(id);
            String testEmail ="contact@aquasys.fr";
            String testNom="Stephane Borton";
            String testMotDePasse= " test";
            Date testDateCreation = new Date() ;
            boolean testIsAdmin = true;
            utilisateur = new Utilisateur(testEmail,testNom,testMotDePasse,testDateCreation,testIsAdmin);
            articleDAO = new ArticleDAO();
            articleDAO.delArticle(article, utilisateur.getEmail());

        }
        catch (SQLException e){
            System.out.println(e.getMessage());
            assert(e.getMessage().contains("integrity constraint violation: unique constraint or index violation"));
        }
    }

}
