import fr.epsi.myepsi.blog.dal.UtilisateurDAO;
import fr.epsi.myepsi.blog.models.Utilisateur;
import org.junit.Test;

import java.sql.SQLException;
import java.util.Date;

public class TestUtilisateur {
    UtilisateurDAO daoUser;
    Utilisateur user;
    Utilisateur newUser;

    @Test
    public void testDeleteUser(){
    try {


        String email = "contact@aquasys.fr";
        String nom = "Stephane Borton";
        String motDePasse = " test";
        Date dateCreation = new Date();
        boolean isAdmin = true;
        user = new Utilisateur(email, nom, motDePasse, dateCreation, isAdmin);
        daoUser= new UtilisateurDAO();
        daoUser.delUser(user);
    }
    catch (SQLException e){
        assert(e.getMessage().contains("Vous ne pouvez pas supprimer un compte administrateur"));

    }
    }
    @Test
    public void testAddUser  () {
        try {

            String email ="contact@aquasys.fr";
            String nom="Stephane Borton";
            String motDePasse= " test";
            Date dateCreation = new Date() ;
            boolean isAdmin = true;
            user = new Utilisateur(email,nom,motDePasse,dateCreation,isAdmin);
            newUser = new Utilisateur(email,nom,motDePasse,dateCreation,isAdmin);
            daoUser= new UtilisateurDAO();
            daoUser.addUser(user);
            daoUser.addUser(newUser);
        } catch (SQLException e) {
            assert(e.getMessage().contains("integrity constraint violation: unique constraint or index violation"));
        }


    }
}




