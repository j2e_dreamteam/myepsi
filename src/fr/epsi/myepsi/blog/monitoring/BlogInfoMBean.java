package fr.epsi.myepsi.blog.monitoring;

import java.sql.SQLException;

public interface BlogInfoMBean {
    int getNbPosts() throws SQLException;
}
