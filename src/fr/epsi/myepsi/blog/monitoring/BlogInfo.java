package fr.epsi.myepsi.blog.monitoring;

import fr.epsi.myepsi.blog.dal.DAOFactory;

import java.sql.SQLException;

public class BlogInfo implements BlogInfoMBean {
    @Override
    public int getNbPosts() {
        int nbPosts = 0;

        try {
            nbPosts = DAOFactory.getArticleDAO().getArticles(Integer.MAX_VALUE).length;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return nbPosts;
    }
}
