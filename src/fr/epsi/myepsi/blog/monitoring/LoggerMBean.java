package fr.epsi.myepsi.blog.monitoring;

public interface LoggerMBean {
    String getLogLevel();
    void debug();
    void info();
    void error();
}
