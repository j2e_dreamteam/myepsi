package fr.epsi.myepsi.blog.monitoring;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

public class Logger implements LoggerMBean {

    private org.apache.log4j.Logger rootLogger = LogManager.getRootLogger();

    @Override
    public String getLogLevel() {
        return rootLogger.getLevel().toString();
    }

    @Override
    public void debug() {
        rootLogger.setLevel(Level.DEBUG);
    }

    @Override
    public void info() {
        rootLogger.setLevel(Level.INFO);
    }

    @Override
    public void error() {
        rootLogger.setLevel(Level.ERROR);
    }
}
