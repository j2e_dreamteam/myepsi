package fr.epsi.myepsi.blog.helpers;

public class JDBCConstants
{

    public static final String DRIVER_NAME = "org.hsqldb.jdbc.JDBCDriver";
    public static final String URL         = "jdbc:hsqldb:hsql://localhost:9003/";
    public static final String USERNAME    = "SA";
    public static final String PASSWORD    = "";

}
