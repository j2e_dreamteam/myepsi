package fr.epsi.myepsi.blog.helpers;

import fr.epsi.myepsi.blog.models.Utilisateur;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class PermissionHelper {

    public static boolean isAdmin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Utilisateur utilisateur = (Utilisateur) session.getAttribute("sessionUtilisateur");

        return utilisateur != null && utilisateur.getIsAdmin();
    }

    public static boolean isCurrentUser(HttpServletRequest request, String email) {
        HttpSession session = request.getSession();
        Utilisateur utilisateur = (Utilisateur) session.getAttribute("sessionUtilisateur");


        return utilisateur != null && email.equals(utilisateur.getEmail());
    }

}
