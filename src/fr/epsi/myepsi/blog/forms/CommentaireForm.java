package fr.epsi.myepsi.blog.forms;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.models.Commentaire;
import fr.epsi.myepsi.blog.models.Utilisateur;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

public class CommentaireForm {
    private static final String CHAMP_COMMENTAIRE  = "commentaire";

    public Commentaire commenterPost(HttpServletRequest request) throws SQLException, ClassNotFoundException {
        String commentaire = getValeurChamp(request, CHAMP_COMMENTAIRE);

        HttpSession session = request.getSession();

        return new Commentaire(
                UUID.randomUUID().toString(),
                commentaire,
                ((Utilisateur)session.getAttribute("sessionUtilisateur")).getEmail(),
                new Date(),
                DAOFactory.getArticleDAO().getArticle(request.getParameter("ID"))
        );
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
