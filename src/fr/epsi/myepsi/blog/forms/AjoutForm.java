package fr.epsi.myepsi.blog.forms;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.models.Article;
import fr.epsi.myepsi.blog.models.Utilisateur;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class AjoutForm {
    private static final String CHAMP_TITRE = "titre";
    private static final String CHAMP_DESCRIPTION  = "description";
    private static final String CHAMP_STATUT = "statut";

    private String resultat;
    private Map<String, String> erreurs = new HashMap<String, String>();

    public String getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public Article ajouterArticle(HttpServletRequest request ) throws SQLException, ClassNotFoundException {
        /* Récupération des champs du formulaire */
        String titre = getValeurChamp( request, CHAMP_TITRE );
        String description = getValeurChamp( request, CHAMP_DESCRIPTION );
        String statut = getValeurChamp( request, CHAMP_STATUT );

        try {
            validationTitre(titre);
        } catch (Exception e) {
            setErreur( CHAMP_TITRE, e.getMessage() );
        }


        /* Initialisation du résultat global de la validation. */
        if ( erreurs.isEmpty() ) {
            resultat = "Succès de l'ajout de l'article.";
        } else {
            resultat = "Échec de l'ajout de l'article.";
        }

        HttpSession session = request.getSession();

        return new Article(
                titre,
                description,
                ((Utilisateur)session.getAttribute("sessionUtilisateur")).getEmail(),
                new Date(),
                new Date(),
                DAOFactory.getStatutDAO().getStatut(Integer.parseInt(statut))
        );
    }

    public void modifierArticle(Article article, HttpServletRequest request) {
        article.setTitre(getValeurChamp( request, CHAMP_TITRE ));
        article.setDescription(getValeurChamp(request, CHAMP_DESCRIPTION));
        try {
            article.setStatut(DAOFactory.getStatutDAO().getStatut(Integer.parseInt(getValeurChamp(request, CHAMP_STATUT))));
        } catch (SQLException e) {
            Logger.getLogger(this.getClass()).error("Impossible de récupérer le statut depuis la base de données");
        }
    }

    private void validationTitre(String titre) throws Exception {
        if (titre == null) {
            throw new Exception("Le titre ne doit pas etre vide.");
        }

        if (titre.length() > 60) {
            throw new Exception( "Le titre doit faire moins de 60 caractères :-(" );
        }
    }
    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
