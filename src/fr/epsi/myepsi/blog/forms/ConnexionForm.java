package fr.epsi.myepsi.blog.forms;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.models.Utilisateur;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public final class ConnexionForm {
    private static final String CHAMP_EMAIL  = "email";
    private static final String CHAMP_PASS   = "motdepasse";

    private String              resultat;
    private Map<String, String> erreurs      = new HashMap<String, String>();

    public String getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public Utilisateur connecterUtilisateur(HttpServletRequest request ) throws SQLException {
        /* Récupération des champs du formulaire */
        String email = getValeurChamp( request, CHAMP_EMAIL );
        String motDePasse = getValeurChamp( request, CHAMP_PASS );

        try {
            validationEmail(email, motDePasse);
        } catch ( Exception e ) {
            setErreur( CHAMP_EMAIL, e.getMessage() );
        }

        Utilisateur utilisateur = DAOFactory.getConnexionDAO().getUtilisateur(email);

        try {
            validationUtilisateur(utilisateur, motDePasse);
        } catch (Exception e) {
            setErreur(CHAMP_PASS, e.getMessage());
        }

        /* Initialisation du résultat global de la validation. */
        if ( erreurs.isEmpty() ) {
            resultat = "Succès de la connexion. Redez-vous sur la page d'<a href=\"accueil\">accueil</a>";
        } else {
            Logger.getLogger(this.getClass()).error("Une tentative d'authentification ratée a été effectuée avec l'utilisateur suivant : " + email);
            resultat = "Échec de la connexion.";
        }

        return utilisateur;
    }

    /**
     * Valide l'email.
     */
    private void validationEmail(String email, String pass) throws Exception {
        if ( email != null && !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
            throw new Exception( "Merci de saisir une adresse mail valide." );
        }
    }
    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    /*
        Valide la connexion de l'utilisateur
     */
    private void validationUtilisateur(Utilisateur utilisateur, String motDePasse) throws Exception {
        if (utilisateur.getMotDePasse().equals(motDePasse)) {

        } else {
            throw new Exception("Merci d'utiliser un couple utilisateur/mot de passe valide");
        }
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
