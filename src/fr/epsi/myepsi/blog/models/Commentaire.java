package fr.epsi.myepsi.blog.models;

import java.util.Date;

public class Commentaire {
    private String id;
    private String commentaire;
    private String email;
    private Date dateCreation;
    private Article post;

    public Commentaire(String id, String commentaire, String email, Date dateCreation, Article post) {
        this.id = id;
        this.commentaire = commentaire;
        this.email = email;
        this.dateCreation = dateCreation;
        this.post = post;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Article getPost() {
        return post;
    }

    public void setPost(Article post) {
        this.post = post;
    }
}
