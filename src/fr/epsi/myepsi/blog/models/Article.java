package fr.epsi.myepsi.blog.models;

import java.util.Date;

public class Article {
    private String id;
    private String titre;
    private String description;
    private String email;
    private Date dateCreation;
    private Date dateModification;
    private Statut statut;

    public Article(String titre, String description, String email, Date dateCreation, Date dateModification, Statut statut) {
        this.titre = titre;
        this.description = description;
        this.email = email;
        this.dateCreation = dateCreation;
        this.dateModification = dateModification;
        this.statut = statut;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }
}
