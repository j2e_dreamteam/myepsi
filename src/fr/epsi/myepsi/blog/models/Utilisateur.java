package fr.epsi.myepsi.blog.models;

import java.util.Date;

public class Utilisateur {
    private String email;
    private String nom;
    private String motDePasse;
    private Date dateCreation;
    private boolean isAdmin;

    public Utilisateur(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Utilisateur(String email, String nom, String motDePasse, Date dateCreation, boolean isAdmin) {
        this.email = email;
        this.nom = nom;
        this.motDePasse = motDePasse;
        this.dateCreation = dateCreation;
        this.isAdmin = isAdmin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
}
