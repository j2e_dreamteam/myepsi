package fr.epsi.myepsi.blog.dal;

import fr.epsi.myepsi.blog.helpers.JDBCHelper;
import fr.epsi.myepsi.blog.models.Statut;

import java.sql.*;
import java.util.ArrayList;

public class StatutDAO {
    private static final String SELECT_STATUTS = "SELECT * FROM STATUT";
    private static final String SELECT_STATUT = "SELECT * FROM STATUT WHERE ID = ?";

    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    StatutDAO() {
        con = null;
        ps = null;
        rs = null;
    }

    public Statut getStatut(int id) throws SQLException {
        Statut statut = new Statut(0, "Indéfini");

        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(SELECT_STATUT);
        ps.setInt(1, id);
        rs = ps.executeQuery();

        if (rs.next()) {
            statut = new Statut(
                    rs.getInt("ID"),
                    rs.getString("TITLE")
            );
        }

        return statut;
    }

    public Statut[] getStatuts() throws SQLException {
        ArrayList<Statut> statuts = new ArrayList<>();

        con = JDBCHelper.getConnection();
        ps = con.prepareStatement( SELECT_STATUTS );
        rs = ps.executeQuery();

        while (rs.next()) {
            statuts.add(
                new Statut(
                        rs.getInt("ID"),
                        rs.getString("TITLE")
                )
            );
        }

        return statuts.toArray(new Statut[0]);
    }

}
