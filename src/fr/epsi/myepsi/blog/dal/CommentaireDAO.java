package fr.epsi.myepsi.blog.dal;

import fr.epsi.myepsi.blog.helpers.JDBCHelper;
import fr.epsi.myepsi.blog.models.Commentaire;

import java.sql.*;
import java.util.ArrayList;

public class CommentaireDAO {
    private static final String SELECT_COMMENTAIRES_OF_POST = "SELECT * FROM BLOG_COMMENTAIRES WHERE POST = ?";
    private static final String ADD_COMMENTAIRE = "INSERT INTO BLOG_COMMENTAIRES VALUES (?, ?, ?, ?, ?)";

    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    CommentaireDAO() {
        con = null;
        ps = null;
        rs = null;
    }

    public void addCommentaire(Commentaire commentaire) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(ADD_COMMENTAIRE);
        ps.setString(1, commentaire.getId());
        ps.setString(2, commentaire.getCommentaire());
        ps.setString(3, commentaire.getEmail());
        ps.setDate(4, new Date(commentaire.getDateCreation().getTime()));
        ps.setString(5, commentaire.getPost().getId());
        ps.executeUpdate();
    }

    public Commentaire[] getCommentaires(String post) throws SQLException {
        ArrayList<Commentaire> commentaires = new ArrayList<>();

        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(SELECT_COMMENTAIRES_OF_POST);
        ps.setString(1, post);
        rs = ps.executeQuery();

        while (rs.next()) {
            Commentaire commentaireToAdd = new Commentaire(
                    rs.getString("ID"),
                    rs.getString("COMMENTAIRE"),
                    rs.getString("EMAIL"),
                    rs.getDate("DATE_CREATION"),
                    DAOFactory.getArticleDAO().getArticle(rs.getString("POST"))
            );

            commentaires.add(commentaireToAdd);
        }

        return commentaires.toArray(new Commentaire[0]);
    }
}
