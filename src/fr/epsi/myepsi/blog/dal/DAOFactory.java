package fr.epsi.myepsi.blog.dal;

public class DAOFactory {

    private DAOFactory() {
        throw new IllegalStateException("Utility class");
    }

    public static UtilisateurDAO getConnexionDAO() {
        return new UtilisateurDAO();
    }

    public static ArticleDAO getArticleDAO() {
       return new ArticleDAO();
    }

    public static StatutDAO getStatutDAO() {
        return new StatutDAO();
    }

    public static UtilisateurDAO getUtilisateurDAO() {
        return new UtilisateurDAO();
    }

    public static CommentaireDAO getCommentaireDAO() {
        return new CommentaireDAO();
    }
}
