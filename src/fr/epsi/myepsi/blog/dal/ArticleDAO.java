package fr.epsi.myepsi.blog.dal;

import fr.epsi.myepsi.blog.helpers.JDBCHelper;
import fr.epsi.myepsi.blog.models.Article;
import fr.epsi.myepsi.blog.models.Statut;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class ArticleDAO {
    private static final String SELECT_ARTICLES = "SELECT * FROM BLOG LIMIT ?";
    private static final String SELECT_ARTICLES_BY_STATUS = "SELECT * FROM BLOG WHERE STATUT = ? LIMIT ?";
    private static final String SELECT_ARTICLE = "SELECT * FROM BLOG WHERE ID = ?";
    private static final String ADD_ARTICLE = "INSERT INTO BLOG VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String DEL_ARTICLE = "DELETE FROM BLOG WHERE ID = ? AND EMAIL = ?";
    private static final String UPDATE_ARTICLE = "UPDATE BLOG SET TITRE=?, DESCRIPTION=?, STATUT=?, DATE_MODIFICATION=NOW() WHERE ID=?";

    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public ArticleDAO() {
        con = null;
        ps = null;
        rs = null;
    }

    public void addArticle(Article article) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(ADD_ARTICLE);
        ps.setString(1, UUID.randomUUID().toString());
        ps.setString(2, article.getTitre());
        ps.setString(3, article.getDescription());
        ps.setString(4, article.getEmail());
        ps.setDate(5, new java.sql.Date(article.getDateCreation().getTime()));
        ps.setDate(6, new java.sql.Date(article.getDateModification().getTime()));
        ps.setInt(7, article.getStatut().getId());
        ps.executeUpdate();
    }

    public Article[] getArticles(int nb) throws SQLException {
        ArrayList<Article> articles = new ArrayList<>();

        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(SELECT_ARTICLES);
        ps.setInt(1, nb);
        rs = ps.executeQuery();

        while (rs.next()) {
            Article articleToAdd = createArticleFromResultSet();

            articles.add(articleToAdd);
        }

        return articles.toArray(new Article[0]);
    }

    public Article[] getArticles(int nb, Statut statut) throws SQLException {
        ArrayList<Article> articles = new ArrayList<>();

        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(SELECT_ARTICLES_BY_STATUS);
        ps.setInt(1, statut.getId());
        ps.setInt(2, nb);
        rs = ps.executeQuery();

        while (rs.next()) {
            Article articleToAdd = createArticleFromResultSet();

            articles.add(articleToAdd);
        }

        return articles.toArray(new Article[0]);
    }


    public Article getArticle(String uuid) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(SELECT_ARTICLE);
        ps.setString(1, uuid);
        rs = ps.executeQuery();

        if (rs.next()) {
            return createArticleFromResultSet();
        } else {
            return null;
        }
    }

    private Article createArticleFromResultSet() throws SQLException {
        Article articleToAdd = new Article(
                rs.getString("TITRE"),
                rs.getString("DESCRIPTION"),
                rs.getString("EMAIL"),
                rs.getDate("DATE_CREATION"),
                rs.getDate("DATE_MODIFICATION"),
                DAOFactory.getStatutDAO().getStatut(rs.getInt("STATUT"))
        );

        articleToAdd.setId(rs.getString("ID"));
        return articleToAdd;
    }

    public void delArticle(Article article, String email) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(DEL_ARTICLE);
        ps.setString(1, article.getId());
        ps.setString(2, email);
        ps.executeUpdate();
    }

    public void updateArticle(String uid, Article article) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(UPDATE_ARTICLE);
        ps.setString(1, article.getTitre());
        ps.setString(2, article.getDescription());
        ps.setInt(3, article.getStatut().getId());
        ps.setString(4, uid);
        ps.executeUpdate();
    }
}
