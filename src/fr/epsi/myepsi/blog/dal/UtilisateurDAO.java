package fr.epsi.myepsi.blog.dal;

import fr.epsi.myepsi.blog.helpers.JDBCHelper;
import fr.epsi.myepsi.blog.models.Utilisateur;

import java.sql.*;
import java.util.ArrayList;

public class UtilisateurDAO {
    private static final String SELECT_USER = "SELECT * FROM USERS WHERE EMAIL = ?";
    private static final String SELECT_USERS = "SELECT * FROM USERS";
    private static final String ADD_USER = "INSERT INTO USERS VALUES (?, ?, NOW(), ?, ?)";
    private static final String DELETE_USER = "DELETE FROM USERS WHERE EMAIL = ?";
    private static final String UPDATE_USER = "UPDATE USERS SET NOM=?, DATE_CREATION=?, PASSWORD=?, IS_ADMIN=? WHERE EMAIL=?";

    private Connection con;
    private PreparedStatement ps;
    private ResultSet rs;

    public UtilisateurDAO() {
        con = null;
        ps = null;
        rs = null;
    }

    public void addUser(Utilisateur user) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(ADD_USER);
        ps.setString(1, user.getEmail());
        ps.setString(2, user.getNom());
        ps.setString(3, user.getMotDePasse());
        ps.setBoolean(4, user.getIsAdmin());
        ps.executeUpdate();
    }

    public void delUser(Utilisateur user) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(DELETE_USER);
        ps.setString(1, user.getEmail());
        ps.executeUpdate();
    }

    public void updateUser(Utilisateur user) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(UPDATE_USER);
        ps.setString(1, user.getNom());
        ps.setDate(2, new Date(user.getDateCreation().getTime()));
        ps.setString(3, user.getMotDePasse());
        ps.setBoolean(4, user.getIsAdmin());
        ps.setString(5, user.getEmail());
        ps.executeUpdate();
    }

    public Utilisateur getUtilisateur(String email) throws SQLException {
        con = JDBCHelper.getConnection();
        ps = con.prepareStatement( SELECT_USER );
        ps.setString(1, email);
        rs = ps.executeQuery();

        if (rs.next()) {
            return createUserFromResultSet();
        }

        return null;
    }

    public Utilisateur[] getUtilisateurs() throws SQLException {
        ArrayList<Utilisateur> utilisateurs = new ArrayList<>();

        con = JDBCHelper.getConnection();
        ps = con.prepareStatement(SELECT_USERS);
        rs = ps.executeQuery();

        while (rs.next()) {
            Utilisateur userToAdd = createUserFromResultSet();

            utilisateurs.add(userToAdd);
        }

        return utilisateurs.toArray(new Utilisateur[0]);
    }

    private Utilisateur createUserFromResultSet() throws SQLException {
        return new Utilisateur(
                rs.getString("EMAIL"),
                rs.getString("NOM"),
                rs.getString("PASSWORD"),
                rs.getDate("DATE_CREATION"),
                rs.getBoolean("IS_ADMIN")
        );
    }

}
