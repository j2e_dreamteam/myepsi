package fr.epsi.myepsi.blog.config;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.monitoring.BlogInfo;
import fr.epsi.myepsi.blog.monitoring.BlogInfoMBean;
import fr.epsi.myepsi.blog.monitoring.Logger;
import fr.epsi.myepsi.blog.monitoring.LoggerMBean;

import javax.management.*;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.lang.management.ManagementFactory;
import java.sql.SQLException;

@WebListener
public class CustomServletContextListener implements ServletContextListener {

    private ObjectName objectName;
    private org.apache.log4j.Logger logger;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        logger = org.apache.log4j.Logger.getLogger(this.getClass());

        registerCustomJMX();
        logStartup();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        unregisterCustomJMX();
    }

    private void registerCustomJMX() {
        logger.info("Registering MBean...");
        final MBeanServer server = ManagementFactory.getPlatformMBeanServer();

        try {
            objectName = new ObjectName("monitoring:type=LoggerMBean");
            final LoggerMBean mbean = new Logger();

            server.registerMBean(mbean, objectName);

            objectName = new ObjectName("monitoring:type=BlogInfoMBean");
            final BlogInfoMBean mbean2 = new BlogInfo();

            server.registerMBean(mbean2, objectName);
        } catch (MalformedObjectNameException | InstanceAlreadyExistsException | NotCompliantMBeanException | MBeanRegistrationException mone) {
            mone.printStackTrace();
        }
    }

    private void unregisterCustomJMX() {
        logger.info("Unregistering MBean...");
        final MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        try {
            objectName = new ObjectName("monitoring:type=LoggerMBean");
            server.unregisterMBean(objectName);
            logger.info("MBean unregistered: " + objectName);

            objectName = new ObjectName("monitoring:type=BlogInfoMBean");
            server.unregisterMBean(objectName);
            logger.info("MBean unregistered: " + objectName);
        } catch (MalformedObjectNameException | InstanceNotFoundException | MBeanRegistrationException mone) {
            mone.printStackTrace();
        }
    }

    private void logStartup() {
        try {
            int nbPosts = DAOFactory.getArticleDAO().getArticles(Integer.MAX_VALUE).length;
            int nbUsers = DAOFactory.getUtilisateurDAO().getUtilisateurs().length;

            logger.error("Nombre de posts présents dans la base : " + nbPosts);
            logger.error("Nombre d'utilisateurs présents dans la base : " + nbUsers);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
