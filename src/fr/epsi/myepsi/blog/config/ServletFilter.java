package fr.epsi.myepsi.blog.config;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter("/*")
public class ServletFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
        // Unused method implemented by super which is executed at init of Filter
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        Logger.getLogger(this.getClass()).info("Début d'execution d'un servlet");

        filterChain.doFilter(servletRequest, servletResponse) ;
    }

    @Override
    public void destroy() {
        // Unused method implemented by super which is executed at destroy of Filter
    }
}
