package fr.epsi.myepsi.blog.config;

import org.apache.log4j.Logger;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class CustomServletRequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        Logger.getLogger(this.getClass()).debug("Une requete " + servletRequestEvent.getServletRequest().getScheme() + " a été faite.");
    }
}
