package fr.epsi.myepsi.blog.servlets.Article;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.forms.CommentaireForm;
import fr.epsi.myepsi.blog.models.Commentaire;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class Article extends HttpServlet {
    private static final String VUE = "/WEB-INF/article.jsp";

    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        fr.epsi.myepsi.blog.models.Article article = null;

        try {
            article = DAOFactory.getArticleDAO().getArticle(request.getParameter("ID"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Commentaire[] commentaires = null;

        try {
            commentaires = DAOFactory.getCommentaireDAO().getCommentaires(request.getParameter("ID"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("article", article);
        request.setAttribute("commentaires", commentaires);

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        CommentaireForm form = new CommentaireForm();
        try {
            DAOFactory.getCommentaireDAO().addCommentaire(form.commenterPost(request));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        doGet(request, response);
    }
}
