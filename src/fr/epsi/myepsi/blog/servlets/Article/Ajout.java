package fr.epsi.myepsi.blog.servlets.Article;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.forms.AjoutForm;
import fr.epsi.myepsi.blog.models.Article;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class Ajout extends HttpServlet {
    private static final String VUE = "/WEB-INF/article/ajout.jsp";
    private static final String ACTION = "ajouter";

    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        try {
            request.setAttribute("statuts", DAOFactory.getStatutDAO().getStatuts());
            request.setAttribute("action", ACTION);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        AjoutForm form = new AjoutForm();

        /* Traitement de la requête et récupération du bean en résultant */
        Article article = null;
        try {
            article = form.ajouterArticle( request );
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();

        if (session.getAttribute("sessionUtilisateur") != null) {
            if (form.getErreurs().isEmpty()) {
                try {
                    DAOFactory.getArticleDAO().addArticle(article);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        doGet(request, response);
    }
}
