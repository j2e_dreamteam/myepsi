package fr.epsi.myepsi.blog.servlets.Article;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.models.Utilisateur;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

public class Suppression extends HttpServlet {
    private static final String URL_REDIRECTION = "/accueil";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();

        Utilisateur user = (Utilisateur) session.getAttribute("sessionUtilisateur");

        try {
            //if (PermissionHelper.isCurrentUser(request, user.getEmail()) || PermissionHelper.isAdmin(request)) {
                DAOFactory.getArticleDAO().delArticle(DAOFactory.getArticleDAO().getArticle(request.getParameter("id")), user.getEmail());
            //}
        } catch (SQLException e) {
            e.printStackTrace();
        }

        response.sendRedirect(request.getContextPath() + URL_REDIRECTION);
    }
}
