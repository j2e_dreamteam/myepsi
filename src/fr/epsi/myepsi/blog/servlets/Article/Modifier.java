package fr.epsi.myepsi.blog.servlets.Article;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.forms.AjoutForm;
import fr.epsi.myepsi.blog.helpers.PermissionHelper;
import fr.epsi.myepsi.blog.models.Article;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class Modifier extends HttpServlet {
    private static final String VUE = "/WEB-INF/article/ajout.jsp";
    private static final String ACTION = "modifier";

    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        try {
            request.setAttribute("statuts", DAOFactory.getStatutDAO().getStatuts());
            request.setAttribute("action", ACTION + "?id=" + request.getParameter("id"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String id = request.getParameter("id");

        if (id != null) {
            try {
                request.setAttribute("article", DAOFactory.getArticleDAO().getArticle(id));
            } catch (SQLException e) {
                Logger.getLogger(this.getClass()).error("Impossible de récupérer l'article demandé");
            }
        }

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        AjoutForm form = new AjoutForm();

        /* Traitement de la requête et récupération du bean en résultant */
        try {
            Article oldArticle = DAOFactory.getArticleDAO().getArticle(request.getParameter("id"));

            if (PermissionHelper.isCurrentUser(request, oldArticle.getEmail()) || PermissionHelper.isAdmin(request)) {

                form.modifierArticle(oldArticle, request);

                DAOFactory.getArticleDAO().updateArticle(request.getParameter("id"), oldArticle);

            } else {
                Logger.getLogger(this.getClass()).info("Un utilisateur non autorisé à essayé de modifier un post.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Logger.getLogger(this.getClass()).error("Impossible de récupérer l'ancien article depuis la base de données");
        }

        doGet(request, response);
    }
}
