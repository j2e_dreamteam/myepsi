package fr.epsi.myepsi.blog.servlets;

import fr.epsi.myepsi.blog.forms.InscriptionForm;
import fr.epsi.myepsi.blog.models.Utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Inscription extends HttpServlet {
    private static final String ATT_USER = "utilisateur";
    private static final String ATT_FORM = "form";
    private static final String VUE = "/WEB-INF/inscription.jsp";

    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page d'inscription */
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        /* Préparation de l'objet formulaire */
        InscriptionForm form = new InscriptionForm();

        /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
        Utilisateur utilisateur = null;
        try {
            utilisateur = form.inscrireUtilisateur( request );
        } catch (Exception e) {
            e.printStackTrace();
        }

        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute( ATT_FORM, form );
        request.setAttribute( ATT_USER, utilisateur );

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}
