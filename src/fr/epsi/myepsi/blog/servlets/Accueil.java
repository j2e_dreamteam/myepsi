package fr.epsi.myepsi.blog.servlets;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.helpers.PermissionHelper;
import fr.epsi.myepsi.blog.models.Article;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;

public class Accueil extends HttpServlet {
    private static final String VUE = "/index.jsp";

    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        Article[] articles = null;

        try {
            articles = Arrays.stream(DAOFactory.getArticleDAO().getArticles(10))
                    .filter(article ->
                            (article.getStatut().getId() == 2) ||
                                    PermissionHelper.isCurrentUser(request, article.getEmail()) ||
                                    PermissionHelper.isAdmin(request)).toArray(Article[]::new);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("articles", articles);

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}
