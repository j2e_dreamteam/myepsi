package fr.epsi.myepsi.blog.servlets.Admin;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.forms.Admin.AddUser;
import fr.epsi.myepsi.blog.models.Utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class Modifier extends HttpServlet {
    public static final String VUE = "/WEB-INF/admin/add-user.jsp";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Utilisateur utilisateur = null;

        try {
            utilisateur = DAOFactory.getUtilisateurDAO().getUtilisateur(request.getParameter("id"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("utilisateur", utilisateur);

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        /* Préparation de l'objet formulaire */
        AddUser form = new AddUser();

        /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
        try {
            DAOFactory.getUtilisateurDAO().updateUser(form.inscrireUtilisateur( request ));
        } catch (SQLException e) {
            e.printStackTrace();
        }


        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}
