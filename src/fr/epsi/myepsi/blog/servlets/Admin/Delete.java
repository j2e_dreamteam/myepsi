package fr.epsi.myepsi.blog.servlets.Admin;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.models.Utilisateur;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class Delete extends HttpServlet {
    private static final String URL_REDIRECTION = "/admin/users";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            Utilisateur utilisateur = DAOFactory.getUtilisateurDAO().getUtilisateur(request.getParameter("id"));

            if (!utilisateur.getIsAdmin()) {
                DAOFactory.getUtilisateurDAO().delUser(utilisateur);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect(request.getContextPath() + URL_REDIRECTION);
    }
}
