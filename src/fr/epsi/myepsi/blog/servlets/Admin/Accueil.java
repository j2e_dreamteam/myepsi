package fr.epsi.myepsi.blog.servlets.Admin;

import fr.epsi.myepsi.blog.dal.DAOFactory;
import fr.epsi.myepsi.blog.models.Utilisateur;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class Accueil extends HttpServlet {
    public static final String VUE = "/WEB-INF/admin/users.jsp";

    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page de connexion */

        Utilisateur[] utilisateurs = null;

        try {
            utilisateurs = DAOFactory.getUtilisateurDAO().getUtilisateurs();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("users", utilisateurs);

        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}
