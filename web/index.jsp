<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:include page="WEB-INF/header.jsp" />

<%--  <a href="ajouter">--%>

<div class="container" style="padding-top: 2em;">
  <c:if test="${not empty sessionUtilisateur}" >
    <a href="<%=request.getContextPath()+"/ajouter"%>"><button type="button" class="btn btn-primary btn-lg"
                              style="position:fixed;bottom:5px;right:5px;margin:0;padding:5px 3px;">Ajouter un article</button></a>
  </c:if>
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-body">
          <!--/stories-->
          <style>
            #circle
            {
              display: block;
              margin: 1em auto;
              background-size: cover;
              background-repeat: no-repeat;
              -webkit-border-radius: 99em;
              -moz-border-radius: 99em;
              border-radius: 99em;
              border: 5px solid #eee;
              box-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);
            }
          </style>
          <c:forEach items="${articles}" var="article">
            <div class="row">
              <br>
              <div class="col-md-6 ">
                <div class="shadow p-3 mb-5 bg-white rounded card flex-md-row mb-4 box-shadow h-md-250">
                  <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">${article.statut.titre}</strong>
                    <h3 class="mb-0">
                      <a href="<%=request.getContextPath()+"/post"%>?ID=${article.id}" class="text-dark" href="#">${article.titre}</a>
                    </h3>
                    <div class="mb-1 text-muted">${article.dateModification}</div>
                    <p class="card-text mb-auto">${article.description}</p>
                      <c:if test="${sessionUtilisateur.isAdmin || article.email == sessionUtilisateur.email}" >
                          <div class="row">
                          <a href="<%=request.getContextPath()+"/article/modifier"%>?id=${article.id}"><button type="button" class="btn btn-primary">Modifier</button></a>
                          <a href="<%=request.getContextPath()+"/article/supprimer"%>?id=${article.id}"><button type="button" class="btn btn-danger">Supprimer</button></a>
                          </div>
                      </c:if>
                  </div>

                  <div >
                    <img  data-toggle="tooltip" data-placement="top" title="${article.email}" id="circle" style="border-radius: 50%;" alt="" src="https://www.gravatar.com/avatar/${article.email.hashCode()}?d=monsterid" style="width:100px;height:100px" class="img-circle">

                    <figcaption>${article.email}</figcaption>

                  </div>

                </div>

              </div>
              <br><br>

            </div>
          </c:forEach>
        </div>
      </div>
    </div><!--/col-12-->
  </div>
</div>

