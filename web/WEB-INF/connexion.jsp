<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
<div class="container">
    <div class="row">
        <div class="col">
            <form method="post" action="connexion">
                <fieldset>
                    <legend>Connexion</legend>
                    <p>Vous pouvez vous connecter via ce formulaire.</p>
                    <div class="form-group">
                        <label for="email">Adresse email <span class="requis">*</span></label>
                        <input type="email" id="email" name="email" value="<c:out value="${utilisateur.email}"/>" size="20" maxlength="60" class="form-control" />
                        <span class="erreur">${form.erreurs['email']}</span>
                        <br />
                    </div>
                    <div class="form-group">
                        <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                        <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" class="form-control"/>
                        <span class="erreur">${form.erreurs['motdepasse']}</span>
                        <br />
                    </div>

                    <input type="submit" value="Connexion" class="btn btn-primary btn-block sansLabel" />
                    <br />
                    <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
                </fieldset>
            </form>
        </div>
    </div>
</div>