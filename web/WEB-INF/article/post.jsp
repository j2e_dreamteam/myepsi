<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp" />
<form method="post" action="ajouter">
    <fieldset>
        <legend>Article</legend>
        <p>Vous pouvez ajouter un article via ce formulaire.</p>

        <label for="titre">Titre <span class="requis">*</span></label>
        <input type="text" id="titre" name="titre" size="20" maxlength="20"/>
        <span class="erreur">${form.erreurs['titre']}</span>
        <br />

        <label for="description">Description <span class="requis">*</span></label>
        <textarea id="description" name="description" cols="40" rows="5"></textarea>
        <span class="erreur">${form.erreurs['description']}</span>
        <br />

        <label for="statut">Statut <span class="requis">*</span></label>
        <select id="statut" name="statut" size="1">
            <c:forEach items="${statuts}" var="statut">
                <option value="${statut.id}">${statut.titre}</option>
            </c:forEach>
        </select>
        <span class="erreur">${form.erreurs['statut']}</span>
        <br />

        <input type="submit" value="Ajouter" class="sansLabel" />
        <br />

        <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
    </fieldset>
</form>
