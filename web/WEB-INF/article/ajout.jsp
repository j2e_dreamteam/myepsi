<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp" />
<div class="container">

    <h4>Ajout d'un article</h4>

    <form role="form" class="clearfix"  method="post" action="${action}">
        <fieldset>
            <div class="col-md-6 form-group">
                <label class="sr-only" for="titre">Titre</label>
                <input type="text" class="form-control" id="titre" value="${article.titre}" name="titre" placeholder="Titre">
            </div>

            <div class="col-md-6 form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="statut">Status</label>
                    </div>
                    <select id="statut" name="statut" class="custom-select" id="statut">
                        <c:forEach items="${statuts}" var="statut">
                            <option <c:if test="${statut.id == article.statut.id}">selected</c:if> value="${statut.id}">${statut.titre}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="col-md-12 form-group">
                <label class="sr-only" for="description">Description</label>
                <textarea class="form-control" id="description" name="description" placeholder="Commentaire">${article.description}</textarea>
            </div>

            <div class="col-md-12 form-group text-right">
                <button type="submit" value="Ajouter" class="btn btn-primary">Ajouter/Modifier</button>
            </div>
            <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
        </fieldset>
    </form>
</div>
