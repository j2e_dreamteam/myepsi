<%--
  Created by IntelliJ IDEA.
  User: simon
  Date: 25/03/19
  Time: 18:27
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="../header.jsp" />
<table>
    <thead>
        <tr>
            <th>Email</th>
            <th>Nom</th>
            <th>Date de créa.</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${users}" var="user">
            <c:if test="${not user.isAdmin}" >
            <tr>
            </c:if>
            <c:if test="${user.isAdmin}" >
            <tr class="admin">
            </c:if>
                <td>${user.email}</td>
                <td>${user.nom}</td>
                <td>${user.dateCreation}</td>
                <td>
                    <a href="admin-modify?id=${user.email}"><button class="btn btn-primary"> Modifier </button></a>
                    <c:if test="${not user.isAdmin}" ><a href="admin-delete?id=${user.email}"><button class="btn btn-primary">Supprimer</button></a></c:if>
                </td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<p><a href="admin-add">Ajouter un utilisateur</a></p>
