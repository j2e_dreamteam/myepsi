<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:include page="../header.jsp" />
<div class="container">
<form method="post" action="">

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Adresse email</span>
        </div>
        <input  value="<c:out value="${utilisateur.email}"/>" id="email" name="email"  type="text" class="form-control" placeholder="Adresse mail" aria-label="Username" aria-describedby="basic-addon1">
    </div>
    <span class="erreur">${form.erreurs['email']}</span>

<br />

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Mot de passe </span>
        </div>
        <input  id="motdepasse" name="motdepasse"  type="password" class="form-control" placeholder="Mot de passe" aria-label="Mot de passe" aria-describedby="basic-addon1">
    </div>
    <span class="erreur">${form.erreurs['motdepasse']}</span>
<br />

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" >Nom d'utilisateur</span>
        </div>
        <input id="nom" name="nom" value="<c:out value="${utilisateur.nom}"/>"  type="text" class="form-control" placeholder="Utilisateur" aria-label="Username" aria-describedby="basic-addon1">
    </div>
    <span class="erreur">${form.erreurs['nom']}</span>
<br />
    <label for="admin">Administrateur
        <p>
    <input class="form-control"  type="checkbox" id="admin" name="admin" value="<c:out value="${utilisateur.isAdmin}"/>" />
        </p>
    </label>
        <span class="erreur">${form.erreurs['admin']}</span>
    <br />

<input type="submit" value="Ajout de l'utilisateur" class="sansLabel" />
</form>
<br />
</div>