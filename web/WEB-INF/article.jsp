<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
<div class="container" style="padding-top: 2em;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <!--/stories-->
                    <style>
                        #circle
                        {
                            display: block;
                            margin: 1em auto;
                            background-size: cover;
                            background-repeat: no-repeat;
                            -webkit-border-radius: 99em;
                            -moz-border-radius: 99em;
                            border-radius: 99em;
                            border: 5px solid #eee;
                            box-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);
                        }
                    </style>
                        <div class="row">
                            <br>
                            <div class="col-md-6 ">
                                <div class="shadow p-3 mb-5 bg-white rounded card flex-md-row mb-4 box-shadow h-md-250">
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-success">${article.statut.titre}</strong>
                                        <h3 class="mb-0">
                                            ${article.titre}
                                        </h3>
                                        <div class="mb-1 text-muted">${article.dateModification}</div>
                                        <p class="card-text mb-auto">${article.description}</p>
                                    </div>

                                    <div >
                                        <img  data-toggle="tooltip" data-placement="top" title="${article.email}" id="circle" style="border-radius: 50%;" alt="" src="https://www.gravatar.com/avatar/${article.email.hashCode()}?d=monsterid" style="width:100px;height:100px" class="img-circle">

                                        <figcaption>${article.email}</figcaption>

                                    </div>

                                </div>

                            </div>
                        </div>
                </div>
            </div>
        </div><!--/col-12-->
    </div>
    <h3 class="mb-0">Commentaires :</h3>
    <c:forEach items="${commentaires}" var="commentaire">
    <div class="container" style="padding-top: 2em;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="row">
                            <br>
                            <div class="col-md-6 ">
                                <div class="shadow p-3 mb-5 bg-white rounded card flex-md-row mb-4 box-shadow h-md-250">
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <p class="card-text mb-auto">${commentaire.commentaire}</p>
                                    </div>
                                    <div >
                                        <div class="mb-1 text-muted">${commentaire.dateCreation}</div>
                                        <div class="mb-1 text-muted">${commentaire.email}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/col-12-->
        </div>
    </div>
    </c:forEach>
    <c:if test="${not empty sessionUtilisateur}" >
        <div class="container">
            <form method="post" action="">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" value="Commenter" type="submit">Ajouter</button>
                    </div>
                    <input  id="add_commentaire" name="commentaire" type="text" class="form-control" placeholder="ajouter un commentaire" aria-label="" aria-describedby="basic-addon1">
                </div>
            </form>
        </div>
    </c:if>
</div>

<br />
