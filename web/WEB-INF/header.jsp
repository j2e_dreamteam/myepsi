<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<c:set var="urlLocal" value="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Index</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   </head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <c:choose>
        <c:when test="${sessionUtilisateur.isAdmin}" >
            <a class="navbar-brand" href="${urlLocal}">Administrateur ${sessionUtilisateur.nom}</a>
        </c:when>
        <c:otherwise>
            <c:if test="${not empty  sessionUtilisateur.nom}"  >
                <a class="navbar-brand" href="${urlLocal}">Blogueur ${sessionUtilisateur.nom}</a>
            </c:if>
            <c:if test="${empty sessionUtilisateur.nom}"  >
                <a class="navbar-brand" href="${urlLocal}">Blog</a>
            </c:if>

        </c:otherwise>
    </c:choose>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>


    <c:if test="${not empty sessionUtilisateur}" >

        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link active" href="${urlLocal}/accueil">Accueil <span class="sr-only">(current)</span></a>

                    <c:if test="${sessionUtilisateur.isAdmin}" >
                        <a class="nav-item nav-link" href="${urlLocal}/admin/users">Administration</a>
                    </c:if>

                <a class="nav-item nav-link" href="${urlLocal}/deconnexion">Deconnexion</a>
            </div>
        </div>
    </c:if>
    <c:if test="${empty sessionUtilisateur}" >
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="${urlLocal}/accueil">Accueil <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="${urlLocal}/connexion">Connexion</a>
            <a class="nav-item nav-link" href="${urlLocal}/inscription">Inscription</a>
        </div>
    </div>
    </c:if>
</nav>






<hr>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</html>
