<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" />
<div class="container">
    <div class="row">
        <div class="col">
            <form method="post" action="inscription">
                <fieldset>
                    <legend>Inscription</legend>
                    <p>Vous pouvez vous inscrire via ce formulaire.</p>
                    <div class="form-group">
                        <label for="email">Adresse email <span class="requis">*</span></label>
                        <input type="email" id="email" name="email" value="<c:out value="${utilisateur.email}"/>" size="20" maxlength="60" class="form-control" />
                        <span class="erreur">${form.erreurs['email']}</span>
                        <br />
                    </div>
                    <div class="form-group">
                        <label for="motdepasse">Mot de passe <span class="requis">*</span></label>
                        <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" class="form-control" />
                        <span class="erreur">${form.erreurs['motdepasse']}</span>
                        <br />
                    </div>
                    <div class="form-group">
                        <label for="confirmation">Confirmation du mot de passe <span class="requis">*</span></label>
                        <input type="password" id="confirmation" name="confirmation" value="" size="20" maxlength="20" class="form-control" />
                        <span class="erreur">${form.erreurs['confirmation']}</span>
                        <br />
                    </div>
                    <div class="form-group">
                        <label for="nom">Nom d'utilisateur <span class="requis">*</span></label>
                        <input type="text" id="nom" name="nom" value="<c:out value="${utilisateur.nom}"/>" size="20" maxlength="20"  class="form-control"/>
                        <span class="erreur">${form.erreurs['nom']}</span>
                        <br />
                    </div>
                    <input type="submit" value="Inscription" class=" btn btn-primary sansLabel" />
                    <br />
                    <p class="${empty form.erreurs ? 'succes' : 'erreur'}">${form.resultat}</p>
                </fieldset>
            </form>
        </div>
    </div>
</div>